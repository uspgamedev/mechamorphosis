extends Area2D

const PART_SELECTOR = preload("res://hud/part_selector/PartSelector.tscn")

export(PackedScene) var new_part_scene = null

var open = false

func interact(interacting_object):
	if !open:
		interacting_object.call_method('open_part_selection_menu')
		open = true
		var selector = PART_SELECTOR.instance()
		selector.entity = interacting_object
		selector.new_part = new_part_scene.instance()
		interacting_object.add_child(selector)
