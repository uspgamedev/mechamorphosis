extends KinematicBody2D

signal updated_hp
signal entity_died

const ACCEL = 50
const FRICTION = .7
const MAX_MOVEMENT = 200

const INVUL_TIME = 2

export var accel_mult = 1.0
export var max_movement_mult = 1.0
export var max_hp = 100
export var def = 0
export var can_be_invulnerable = false

onready var hp = max_hp
onready var invulnerable = false

onready var movement = Vector2()

var movement_enabled = true

func _ready():
	emit_signal("updated_hp", self)


## Helper methods

func find_method(method_name):
	for child in get_children():
		if child.has_method(method_name):
			return child

func call_method(method_name, parameters_array = [], default = null):
	var component = find_method(method_name)
	return component.callv(method_name, parameters_array) if component else default

func call_methods(method_name, parameters_array = []):
	var results = []
	for child in get_children():
		if child.has_method(method_name):
			results.append(child.callv(method_name, parameters_array))
	return results

func get_room_entities():
	return get_parent()

func get_players():
	return get_tree().get_nodes_in_group("Player")

func get_nearest_player():
	var players = get_players()
	var nearest = null
	var dist = -1
	for p in players:
		if dist == -1 or p.position.distance_to(self.position) < dist:
			dist = p.position.distance_to(self.position)
			nearest = p
	return nearest

func get_taunt_player():
	var players = get_players()
	for p in players:
		for part in p.get_children():
			if part.name == "Megaphone":
				return p
	return

## Stats methods

func has_magnet():
	for child in get_children():
		if child.name == "Magnet":
			return true
	return false
	
func has_emp():
	for child in get_children():
		if child.name == "EMP":
			return true
	return false

func _accumulate(value_name):
	var total = 0
	for child in get_children():
		var value = child.get(value_name)
		total += value if value != null else 0
	return total

func get_max_movement():
	var mult_bonus = _accumulate("max_movement_multiplier")
	return MAX_MOVEMENT * self.max_movement_mult * (1 + mult_bonus)

func get_damage_reduction():
	return def + _accumulate("def_bonus")

func get_max_hp():
	var hp_bonus = _accumulate("max_hp_bonus")
	return self.max_hp + hp_bonus

func get_bullet_type():
	for child in get_children():
		var bullet = child.get("bullet_type")
		if bullet != null: return bullet
	return null

func get_bullet_damage_multiplier():
	return _accumulate("bullet_damage_multiplier")

func get_bullet_speed_multiplier():
	return _accumulate("bullet_speed_multiplier")

func get_bullet_hp_bonus():
	return _accumulate("bullet_hp_bonus")

func get_fire_rate_multiplier():
	return _accumulate("fire_rate_multiplier")

func get_spread_bonus():
	return _accumulate("spread_bonus")

func get_burst_rate_bonus():
	return _accumulate("burst_rate_bonus")

func get_burst_quantity_bonus():
	return _accumulate("burst_quantity_bonus")

## Logic methods

func _end_invulnerability():
	self.invulnerable = false

func take_damage(amount, true_damage = null):
	if self.invulnerable and not true_damage:
		return false
	else:
		if not true_damage:
			amount = max(amount - get_damage_reduction(), 0)
		if amount == 0:
			return false
		self.hp -= amount
		emit_signal("updated_hp", self)
		if can_be_invulnerable and not true_damage:
			self.invulnerable = true
			# warning-ignore:return_value_discarded
			get_tree().create_timer(INVUL_TIME).connect("timeout", self, "_end_invulnerability", \
														[], Timer.CONNECT_ONESHOT)
		if has_node("DamageAnim") and not true_damage:
			get_node("DamageAnim").play("damage")
		return true

func heal(amount):
		if self.hp >= get_max_hp():
			return false
		self.hp = min(self.hp + amount, get_max_hp())
		emit_signal("updated_hp", self)
		return true

func _physics_process(_delta):
	_process_hp()
	_process_movement()

func _process_hp():
	if self.hp <= 0:
		emit_signal("entity_died")
		queue_free()

func _process_movement():
	var new_movement = Vector2()
	if movement_enabled:
		new_movement = call_method("get_movement") * ACCEL * accel_mult
	if new_movement.length() > 0:
		self.movement += new_movement
	else:
		self.movement *= FRICTION
	var max_movement = get_max_movement()
	if self.movement.length() > max_movement:
		self.movement = self.movement.normalized() * max_movement
	self.movement = self.move_and_slide(self.movement)
