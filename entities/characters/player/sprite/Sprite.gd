extends Node2D

onready var original_scale = self.scale.x

func _process(delta):
	var movement = get_parent().movement
	if movement.x > 0:
		self.scale.x = -self.original_scale
	elif movement.x < 0:
		self.scale.x = self.original_scale
	var length = movement.length()
	if length > 5:
		if $AnimationPlayer.current_animation != "walk":
			$AnimationPlayer.play("walk")
		var accel = 1 * length / get_parent().get_max_movement()
		$AnimationPlayer.playback_speed = 1 + accel
	else:
		if $AnimationPlayer.current_animation != "idle":
			$AnimationPlayer.play("idle")
		$AnimationPlayer.playback_speed = 1