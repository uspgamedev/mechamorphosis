extends Node2D

onready var original_scale = self.scale.x

func _process(delta):
	var movement = get_parent().get_parent().movement
	if movement.x > 0:
		self.scale.x = -self.original_scale
	elif movement.x < 0:
		self.scale.x = self.original_scale