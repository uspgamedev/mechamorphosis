extends Node

onready var timer = get_node("../Timer")
onready var player = get_node("../../.")
onready var tileset = get_node("/root/Game/Room/Tiles")
onready var area = get_node("../Area2D")

func activate():
	if timer.is_stopped():
		timer.start()
#		player.position = Vector2(randf()*1920, randf()*1080)
		area.position = Vector2(randf()*1920, randf()*1080) - player.position
		while area.get_overlapping_bodies().size() > 1:
			area.position = Vector2(randf()*1920, randf()*1080) - player.position
		player.position = area.position
