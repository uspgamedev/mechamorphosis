extends Node

onready var timer = get_node("../Timer")
onready var cooldown = get_node("../Cooldown")
onready var player = get_node("../../.")

var mem = 1.0
var mem_accel = 1.0

func activate():
	if cooldown.is_stopped():
		cooldown.start()
		timer.start()
		mem = player.max_movement_mult
		player.max_movement_mult = 10.0
		mem_accel = player.accel_mult
		player.accel_mult = 10.0

func _on_Timer_timeout():
	player.max_movement_mult = mem
	player.accel_mult = mem_accel
