All icon files were obtained from game-icons.net under the Creative Commons BY 3.0 license, with credits to Lorc, Delapouite and other contributors (as per game-icons.net/about.html#authors).
