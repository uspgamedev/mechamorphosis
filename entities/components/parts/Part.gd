extends Node

export(String) var title = "Contraption"
export(String, MULTILINE) var description = "It smells"
export(Texture) var icon = preload("res://icon.png")

# Defense bonuses

export(int, -20, 20) var def_bonus = 0
export(int, -1000, 1000) var max_hp_bonus = 0
export(int, -1000, 1000) var regen_bonus = 0

# Offense bonuses

export(PackedScene) var bullet_type = null
export(float, -10.0, 10.0) var bullet_damage_multiplier = 0
export(float, -10.0, 10.0) var bullet_speed_multiplier = 0
export(float, 0.0, 10.0) var bullet_hp_bonus = 0

export(float, -20.0, 10.0) var fire_rate_multiplier = 0
export(int, 0, 90) var spread_bonus = 0
export(float, 0.0, 1.0) var burst_rate_bonus = 0
export(int, 0, 100) var burst_quantity_bonus = 0

# Mobility bonuses

export(float, -1.0, 2.0) var max_movement_multiplier = 0

# Run-time attributes

export(int, 1, 3) var slot_index = null

func _ready():
	if self.slot_index == null:
		self.slot_index = 1
		var entity = get_parent()
		var check = true
		while check and self.slot_index <= 3:
			check = false
			for component in entity.get_children():
				if component is get_script() and component != self:
					var other_index = component.slot_index if component.slot_index != null else -1
					if other_index == self.slot_index:
						self.slot_index = other_index + 1
						check = true
		assert(self.slot_index <= 3)
	if regen_bonus != 0 and $Timer:
		$Timer.connect('timeout', self, 'regen', [regen_bonus])

func regen(amount):
	if amount > 0:
		get_parent().heal(amount)
	elif amount < 0:
		get_parent().take_damage(-amount, true)

func activate():
	if has_node("Activatable"):
		$Activatable.activate()
