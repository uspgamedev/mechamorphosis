extends Node

export (Vector2) var direction

func get_movement():
	var players = get_parent().get_players()
	for player in players:
		if player.has_magnet():
			var dir = player.position - get_parent().position
			direction = direction.linear_interpolate(dir, .005)
	return direction