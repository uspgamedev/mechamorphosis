extends Node

onready var game = get_parent().get_parent()
onready var alreadyDied = false
onready var game_over_popup = $GameOverPopup

func prepare():
	var players = game.players
	for player in players:
		player.connect("entity_died", self, "die")
	
func die():
	alreadyDied = true
	game_over_popup.show()
	game_over_popup.get_node("AudioStreamPlayer").play()