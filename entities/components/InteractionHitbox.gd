extends Area2D

func _physics_process(delta):
	var movement = get_parent().call_method("get_look_direction")
	if get_parent().call_method("has_interacted"):
		var interacted = get_interactable()
		if interacted != null:
			interacted.interact(get_parent())

func get_interactable():
	var areas = get_overlapping_areas()
	for area in areas:
		if area.has_method("interact"):
			return area
	return null
