extends Node

const PART_SCRIPT = preload("res://entities/components/parts/Part.gd")

signal updated_parts

var parts = []

func get_parts():
	return parts

func update_parts():
	parts = []
	for child in get_parent().get_children():
		if child is PART_SCRIPT:
			parts.append(child)

func add_part(part):
	get_parent().add_child(part)
	emit_signal("updated_parts", get_parent())
	update_parts()

func remove_part(part):
	part.free()
	emit_signal("updated_parts", get_parent())
	update_parts()