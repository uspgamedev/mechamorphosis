extends Node

export (float) var selfDestructTime

func _ready():
	var timer = $SelfDestructTimer
	timer.wait_time = selfDestructTime

func _on_SelfDestructTimer_timeout():
	get_parent().queue_free()