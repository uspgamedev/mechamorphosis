extends Node

export (PackedScene) var default_bullet_type = null
export (float) var fire_rate = 1
export (float, 0, 360) var spread = 30
export (float, 0, 1) var burst_rate = 0
export (int, 1, 100) var burst_quantity = 1

onready var cooldown = $Cooldown

func _physics_process(delta):
	var controller = get_parent().find_method("is_shooting")
	if controller and controller.is_shooting() and cooldown.is_stopped():
		shoot(controller)

func get_bullet_type():
	var override = get_parent().get_bullet_type()
	return override if override != null else self.default_bullet_type

func get_fire_rate():
	return self.fire_rate * (1.0 + get_parent().get_fire_rate_multiplier())

func get_spread():
	return self.spread + get_parent().get_spread_bonus()/2 # it's actually double angle!

func get_burst_rate():
	return self.burst_rate + get_parent().get_burst_rate_bonus()

func get_burst_quantity():
	return self.burst_quantity + get_parent().get_burst_quantity_bonus()

func shoot(controller):
	var cd_time = 1.0 / self.get_fire_rate()
	if(cd_time < 0): cd_time = 1.0
	cooldown.wait_time = cd_time 
	cooldown.start()
	
	var delay = (1 - self.get_burst_rate()) * cooldown.wait_time / self.get_burst_quantity()
	for i in range(0, self.get_burst_quantity()):
		var timer = get_tree().create_timer(i * delay)
		timer.connect("timeout", self, "create_bullet", [controller])

func create_bullet(controller):
	var effect = AudioServer.get_bus_effect(4, 0)
	effect.pitch_scale = .7 + randf()*.6
	var shooter_entity = get_parent()
	var projectile = get_bullet_type().instance()
	var mover = projectile.find_method("get_movement")
	var dir = controller.get_look_direction()
	var angle = deg2rad(get_spread())
	mover.direction = dir.rotated(rand_range(-angle,angle))
	projectile.position = shooter_entity.position + 32 * dir
	projectile.max_hp += shooter_entity.get_bullet_hp_bonus()
	projectile.max_movement_mult *= (1.0 + shooter_entity.get_bullet_speed_multiplier())
	var hitbox = projectile.find_method("deal_damage")
	if hitbox:
		hitbox.damage_power *= (1.0 + shooter_entity.get_bullet_damage_multiplier())
	
	if shooter_entity.get_room_entities():
		shooter_entity.get_room_entities().add_child(projectile)
