extends Node

func _ready():
	var player = get_parent().get_nearest_player()
	var controller = player.find_method("is_shooting")
	var projectileSprite = get_node("../Sprite")
	projectileSprite.rotation_degrees = rad2deg(controller.get_look_direction().angle())