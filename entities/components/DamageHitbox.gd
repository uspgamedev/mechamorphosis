extends Node

const ENTITY = preload("res://entities/Entity.gd")

export(int, 0, 1000) var damage_power = 5

func deal_damage(entity):
	var self_entity = get_parent()
	if entity is ENTITY:
		if entity.take_damage(self.damage_power):
			self_entity.hp -= 1
			var audio = entity.get_node("GotHit")
			audio.play()
			var effect = AudioServer.get_bus_effect(3, 0)
			effect.pitch_scale = .7 + randf()*.6
	else:
		self_entity.hp -= 1

func _on_HitBox_area_entered(area):
	if area.is_in_group('hitbox'):
		deal_damage(area.get_parent())

func _on_HitBox_body_entered(body):
	if body.name == 'Tiles':
		get_parent().queue_free()
