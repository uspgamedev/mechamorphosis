extends Node

const CREDITS_PATH = "res://menus/credits/Credits.tscn"

func _ready():
	get_parent().connect("entity_died", self, "change_to_credits")

func change_to_credits():
	get_tree().change_scene(CREDITS_PATH)