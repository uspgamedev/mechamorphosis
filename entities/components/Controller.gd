extends Node

export var controller_id = 1

var controller = "kb"
var player_id = -1

func get_movement():
	var dir = Vector2()
	
	match controller:
		"kb":
			var left_pressed = Input.is_action_pressed(str("left_" + controller))
			var right_pressed = Input.is_action_pressed(str("right_" + controller))
			var up_pressed = Input.is_action_pressed(str("up_" + controller))
			var down_pressed = Input.is_action_pressed(str("down_" + controller))
			
			if left_pressed:
				dir += Vector2(-1, 0)
			if right_pressed:
				dir += Vector2(1, 0)
			if up_pressed:
				dir += Vector2(0, -1)
			if down_pressed:
				dir += Vector2(0, 1)
		"j1":
			dir = Vector2(Input.get_joy_axis(0, JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1)).normalized()
		"j2":
			dir = Vector2(Input.get_joy_axis(1, JOY_AXIS_0), Input.get_joy_axis(1, JOY_AXIS_1)).normalized()
		
	if get_parent().has_emp():
		dir *= -1
	
	var counter = 0
	var parts = get_parent().call_method("get_parts")
	for part in parts:
		if is_skill_pressed(counter):
			part.activate()
		counter += 1
	
	return dir.normalized()


func get_look_direction():
	match controller:
		"kb": # TODO what is 32?
			return (get_parent().get_global_mouse_position() - get_parent().position + Vector2(0, 32)).normalized()
		"j1":
			return Vector2(Input.get_joy_axis(0, JOY_AXIS_3), Input.get_joy_axis(0, JOY_AXIS_2)).normalized()
		"j2":
			return Vector2(Input.get_joy_axis(1, JOY_AXIS_2), Input.get_joy_axis(1, JOY_AXIS_3)).normalized()


func has_interacted():
	return Input.is_action_just_pressed(str("action_" + controller))


func is_shooting():
	match controller:
		"kb":
			return Input.is_action_pressed(str("shoot_" + controller))
		"j1", "j2":
			return get_look_direction() != Vector2()
	


func is_skill_pressed(skill_number):
	# return Input.is_action_pressed("skill_" + str(skill_number + 1) + "_" + controller)
	return Input.is_action_pressed("skill")


func get_controller_id():
	return controller_id

# Sets the player id and returns if should remove the current player
func set_player_id(value):
	player_id = value
	
	var connected_joypads = Input.get_connected_joypads().size()
	var should_remove = false
	
	if player_id == 2:
		if connected_joypads < 1:
			should_remove = true
		controller = "j1"
		
	if player_id == 3:
		if connected_joypads < 2:
			should_remove = true
		controller = "j2"
		
	if player_id == 4: # TODO implementation of 4th player pending -- missing testing controller
		should_remove = true
		
	return should_remove