extends Node2D

func _ready():
	get_parent().connect("updated_hp", self, "_on_parent_updated_hp")
	$LifeProgressBar.max_value = get_parent().max_hp

func _on_parent_updated_hp(parent):
	$LifeProgressBar.value = parent.hp