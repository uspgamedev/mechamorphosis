extends Node2D

export(float, 0.0, 10000.0) var min_shooting_range = 0.0
export(float, 0.0, 10000.0) var max_shooting_range = 0.0
export(float, 0.0, 10000.0) var seek_range = 0.0
export(float, 0.0, 10000.0) var stop_range = 0.0
export(float, 0.0, 10000.0) var flee_range = 0.0

enum STATE {STOP, SEARCHING}

var state = STATE.SEARCHING
var searching_direction = Vector2(1,1)

func _ready():
	assert(seek_range >= stop_range)
	assert(stop_range >= flee_range)
	assert(min_shooting_range <= max_shooting_range)
	assert(max_shooting_range <= seek_range)
	randomize_searching_direction()

func get_movement():
	var seek_mult = 1
	var player
	var player_dist
	var taunt_player = get_parent().get_taunt_player()
	
	if taunt_player:
		player = taunt_player
		seek_mult = 100000
	else:
		player = get_parent().get_nearest_player()
		
	$RayCast2DtoPlayer.clear_exceptions()
	
	if player:
		player_dist = (player.position - get_parent().position).length()
		$RayCast2DtoPlayer.cast_to =  player.global_position - global_position
		$RayCast2DtoPlayer.add_exception(player)
		$RayCast2DtoPlayer.add_exception(player.get_node('InteractionHitbox'))
		if player_dist >= seek_range*seek_mult:
			player = null
			$RayCast2DtoPlayer.cast_to = Vector2()
	else:
		$RayCast2DtoPlayer.cast_to = Vector2()
		
	$RayCast2DtoPlayer.force_raycast_update()

	if not player or $RayCast2DtoPlayer.is_colliding():
		return get_searching_movement()
	
	if player_dist < flee_range:
		return -get_look_direction(seek_mult)
	elif player_dist < stop_range:
		return Vector2()
	elif player_dist < seek_range*seek_mult:
		return get_look_direction(seek_mult)
		
	$RayCast2DtoPlayer.cast_to = Vector2()
	return get_searching_movement()

func get_searching_movement():
	match state:
		STATE.STOP:
			return Vector2()
		STATE.SEARCHING:
			return searching_direction
	
func randomize_searching_direction():
	searching_direction = searching_direction.rotated(randf() * 1000)

func get_look_direction(mult = 1):
	var player = get_parent().get_nearest_player()
	if not player:
		return Vector2()
	var delta_pos = player.position - get_parent().position
	var player_dist = delta_pos.length()
	if player_dist < seek_range * mult:
		return delta_pos.normalized()
	return Vector2()

func get_taunt_player_look_direction():
	var player = get_parent().get_taunt_player()
	var delta_pos = player.position - get_parent().position
	return delta_pos.normalized()


func is_shooting():
	var player = get_parent().get_nearest_player()
	if not player or $RayCast2DtoPlayer.is_colliding():
		return false
	var player_dist = (player.position - get_parent().position).length()
	return player_dist > min_shooting_range and player_dist < max_shooting_range

func _on_Timer_timeout():
	randomize_searching_direction()
	match state:
		STATE.STOP:
			state = STATE.SEARCHING
		STATE.SEARCHING:
			state = STATE.STOP if randf() > 0.5 else STATE.SEARCHING
