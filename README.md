# MECHAMORPHOSIS

Roguelike multiplayer hot-seat 2D implemented in Godot 3.0 using node-based Entity Component System.

## Code standards

### snake_case

- variables
- functions
- branches

### PascalCase

- files
- nodes