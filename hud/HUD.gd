extends CanvasLayer


func _on_game_start():
	for player in get_node("/root/Game").players:
		var id = player.call_method("get_controller_id")
		#get_node("PlayerHUD%d" % id).visible = true
		var robotic_component = player.find_method("add_part")
		player.connect("updated_hp", self, "_on_player_updated_hp")
		robotic_component.connect("updated_parts", self, "_on_player_updated_parts")
	
	$CheckPlayerDeath.prepare()

func _on_player_updated_hp(player):
	var id = player.call_method("get_controller_id")
	get_node("PlayerHUD%d" % id).update_hp(player.hp)


func _on_player_updated_parts(player):
	var id = player.call_method("get_controller_id")
	get_node("PlayerHUD%d" % id).update_parts(player)
