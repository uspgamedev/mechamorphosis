extends Control

enum STATE {IDLE, HIT, CHANGING}

var value setget set_value
var max_value setget set_max_value

var state = STATE.IDLE

func _ready():
	set_max_value(100)
	set_value(100)

func _physics_process(delta):
	if state == STATE.CHANGING:
		var diff = $ProgressBarBack.value - $ProgressBarFront.value
		if diff != 0:
			$ProgressBarBack.value -= sign(diff)
		else:
			state = STATE.IDLE
			

func set_value(value):
	$ProgressBarFront.value = value
	if state == STATE.IDLE:
		state = STATE.HIT
		$Timer.start()
	

func set_max_value(max_value):
	$ProgressBarBack.max_value = max_value
	$ProgressBarFront.max_value = max_value
	
	

func _on_Timer_timeout():
	state = STATE.CHANGING
