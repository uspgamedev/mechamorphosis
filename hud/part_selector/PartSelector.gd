extends CanvasLayer

var entity = null
var new_part = null

signal done

func _ready():
	$ItemList.setup(self.entity)
	$NewPart/Label.text = "New Item:\n" + new_part.title + '\n' + new_part.description
	$NewPart.get_node("Icon").texture = new_part.icon

func _on_part_chosen(part):
	entity.call_method("remove_part", [part])
	entity.call_deferred("call_method", "add_part", [new_part])
	entity.movement_enabled = true
	entity.get_node("GotItem").play()
	emit_signal("done")
	yield(get_tree().create_timer(0.1), "timeout")
	queue_free()
