extends Control

const PART = preload("res://entities/components/parts/Part.gd")

var parts = [null, null, null]

signal part_chosen(which)

func _ready():
	get_child(0).grab_focus()

func setup(entity):
	for component in entity.get_children():
		if component is PART:
			var slot = get_node("Slot%d" % component.slot_index)
			var label = slot.get_node("Label")
			label.text = component.title
			if(component.description != ""):
				label.text += "\n" + component.description
			slot.get_node("Icon").texture = component.icon
			parts[component.slot_index - 1] = component

func _on_chosen(which):
	$confirm.play()
	emit_signal("part_chosen", parts[which-1])
