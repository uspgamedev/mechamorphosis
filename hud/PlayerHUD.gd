extends Control

const DEFAULT_TEXTURE = preload("res://icon.png")
const PART_SCRIPT = preload("res://entities/components/parts/Part.gd")


func update_parts(player):
	for i in range(1, 4):
		get_node("Skill%d" % i).texture = DEFAULT_TEXTURE
	for child in player.get_children():
		if child is PART_SCRIPT:
			var index = child.slot_index
			if index:
				get_node("Skill%d" % index).texture = child.icon
	$Life.max_value = player.get_max_hp()


func update_hp(hp):
	$Life.value = hp