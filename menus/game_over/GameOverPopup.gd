extends Node

const GAME_PATH = "res://tests/InitialRoomTest.tscn"
const MAIN_MENU_PATH = "res://menus/main_menu/MainMenu.tscn"

func _on_RetryButton_pressed():
	get_tree().change_scene(GAME_PATH)

func _on_MainMenuButton_pressed():
	get_tree().change_scene(MAIN_MENU_PATH)
