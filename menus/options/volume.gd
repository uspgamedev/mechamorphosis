extends Node

enum Buses {MASTER, MUSIC, SFX}

var values = [100, 100, 100]

func _ready():
	pass


func set_volume(bus, value):
	values[bus] = value
	var vol_db = -80
	if value > 0:
		vol_db = 10 * log(value / 100)
	
	AudioServer.set_bus_volume_db(bus, vol_db)
