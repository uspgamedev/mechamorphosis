extends Control

func _ready():
	$Menu/MusicSlider.value = volume.values[volume.MUSIC]
	$Menu/SFXSlider.value = volume.values[volume.SFX]


func _on_MusicSlider_value_changed(value):
	volume.set_volume(volume.MUSIC, value)


func _on_SFXSlider_value_changed(value):
	volume.set_volume(volume.SFX, value)


func _on_Back_pressed():
	get_tree().change_scene("res://menus/main_menu/MainMenu.tscn")
