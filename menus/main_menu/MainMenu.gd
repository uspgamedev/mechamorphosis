extends Control

const CREDITS_PATH = "res://menus/credits/Credits.tscn"
const OPTIONS_PATH = "res://menus/options/Options.tscn"
const GAME_PATH = "res://tests/InitialRoomTest.tscn"

func _ready():
	$Menu/Play.grab_focus()


func _on_Play_pressed():
	get_tree().change_scene(GAME_PATH)


func _on_Options_pressed():
	get_tree().change_scene(OPTIONS_PATH)


func _on_Credits_pressed():
	get_tree().change_scene(CREDITS_PATH)


func _on_Quit_pressed():
	get_tree().quit()
