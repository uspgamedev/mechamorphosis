extends Node2D

const FIRST_ROOM_PATH = "res://rooms/tests/Room1.tscn"

const PLAYER_SCN = preload("res://entities/characters/player/samples/EquippedPlayer.tscn")

var players = []
var actual_room = null
var time_since_changed_room = 0

func _ready():
	start_game()
	
func _physics_process(delta):
	time_since_changed_room += delta

func start_game():
	var transition = $Room/Transition
	var tween = transition.get_node('Tween')
	transition.change_mask()
	tween.interpolate_property(transition, "material:shader_param/cutoff", 1, 0, 2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	
	players = [PLAYER_SCN.instance(), PLAYER_SCN.instance(), PLAYER_SCN.instance(), PLAYER_SCN.instance()]
	var id_counter = 1
	for player in players:
		player.find_method("get_controller_id").controller_id = id_counter
		id_counter += 1
	actual_room = get_node("Room")
	$HUD._on_game_start()
	spawn_players_in_actual_room()
	
	# TODO this doesn't look right
	yield(get_tree(),'physics_frame')
	yield(get_tree(),'physics_frame')
	yield(get_tree(),'physics_frame')
	yield(get_tree(),'physics_frame')
	
	for player in players:
		if player != null:
			$HUD._on_player_updated_parts(player) 


func change_room(room_path):
	if time_since_changed_room < 1:
		return
	time_since_changed_room = 0
	assert(actual_room != null)
	for player in players:
		actual_room.get_node("Entities").remove_child(player)
	actual_room.queue_free()
	var new_room_scene = load(room_path)
	actual_room = new_room_scene.instance()
	spawn_players_in_actual_room()
	add_child(actual_room)
	var transition = actual_room.get_node('Transition')
	var tween = transition.get_node('Tween')
	tween.interpolate_property(transition, "material:shader_param/cutoff", 1.0, 0.0, 2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(get_tree().create_timer(1),'timeout')
	for player in players:
		player.get_node("Sprites").visible = true

func spawn_players_in_actual_room():
	var spawners = []
	for entity in actual_room.get_node('Entities').get_children():
		if entity.is_in_group('PlayerSpawner'):
			spawners.append(entity)
	
	for spawner in spawners:
		assert(spawner.player_id > 0) # make sure the player_id is 1-4
		if players.size() >= spawner.player_id:
			var player = players[spawner.player_id - 1]
			player.position = spawner.position
			yield(get_tree(),'physics_frame')
			actual_room.get_node('Entities').add_child(player)
			player.call_method("update_parts")
			var should_remove_player = player.call_method("set_player_id", [spawner.player_id])
			if should_remove_player:
				player.queue_free()
				players.erase(player)
			else:
				get_node("HUD/PlayerHUD%d" % spawner.player_id).visible = true
