extends Area2D

const PART_SELECTOR = preload("res://hud/part_selector/PartSelector.tscn")

export(PackedScene) var new_part_scene = null

var open = false

func _ready():
	$SpawnAltar.play()

func interact(interacting_object):
	if !open:
		interacting_object.movement_enabled = false
		open = true
		var selector = PART_SELECTOR.instance()
		selector.entity = interacting_object
		selector.new_part = new_part_scene.instance()
		interacting_object.add_child(selector)
		yield(selector, "done")
		for door in get_tree().get_nodes_in_group("Doors"):
			door.open()
		$OpenDoors.play()
