extends Node2D

export (String, FILE, "*.tscn, *.scn") var target_room
export (bool) var locked = false

onready var transition = get_parent().get_parent().get_node("Transition")

func _ready():
	$Area2D/Shape.disabled = true

func open():
	if not self.locked and target_room != null:
		$Area2D/Shape.disabled = false
		$Sprite.animation = "opening"
		$Sign.animation = "unlocked"

func _on_Door_body_entered(body):
	if body.is_in_group("Player"):
		self.locked = true
		var tween = transition.get_node("Tween")
		var dur = 1
		transition.change_mask()
		tween.interpolate_property(transition, "material:shader_param/cutoff", 0, 1, dur, Tween.TRANS_QUAD, Tween.EASE_IN)
		tween.start()
		body.get_node("Sprites").visible = false
		yield(get_tree().create_timer(dur),'timeout')
		get_node("/root/Game").call_deferred('change_room', target_room)
