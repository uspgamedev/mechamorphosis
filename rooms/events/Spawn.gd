extends Position2D

export var delay = 0.0
export var period = 1.0
export var times = 1
export(PackedScene) var entity_scn = null

func _ready():
	yield(get_tree().create_timer(delay), "timeout")
	_spawn()
	$Timer.wait_time = period
	$Timer.start()

func _spawn():
	assert(self.entity_scn != null)
	var entity = self.entity_scn.instance()
	entity.position = self.position
	entity.z_index = 10 + Global.enemy_count
	Global.enemy_count += 1
	if Global.enemy_count > 4000: Global.enemy_count = 1
	get_parent().add_child(entity)
	self.times -= 1
	if self.times <= 0:
		queue_free()
