extends Position2D

export(PackedScene) var altar_scene
export(PackedScene) var new_part_scene

func _physics_process(delta):
	for spawner in get_tree().get_nodes_in_group("Spawners"):
		if spawner.times > 0:
			return
	if get_tree().get_nodes_in_group("Enemies").size() > 0:
		return
	set_physics_process(false)
	var altar = altar_scene.instance()
	altar.new_part_scene = self.new_part_scene
	altar.position = self.position
	get_parent().add_child(altar)
	queue_free()
	
