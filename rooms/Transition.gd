extends ColorRect

var masks = ['curtain', 'from_center', 'shards','clock','cloud','gradient']

func _ready():
	pass

func change_mask():
	var mask = masks[randi()%masks.size()]
	self.get_material().set_shader_param("mask", load(str("res://rooms/transitions/",mask,".png")))

